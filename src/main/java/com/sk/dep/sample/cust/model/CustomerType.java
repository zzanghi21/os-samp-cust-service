package com.sk.dep.sample.cust.model;

public enum CustomerType {

	INDIVIDUAL, COMPANY;
	
}
